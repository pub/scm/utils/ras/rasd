#ifndef __PERF_MAP_H
#define __PERF_MAP_H

#include "symbol.h"

#include <linux/rbtree.h>

enum map_type {
	MAP__FUNCTION = 0,
	MAP__VARIABLE,
};

#define MAP__NR_TYPES (MAP__VARIABLE + 1)

struct map_groups {
	struct rb_root   maps[MAP__NR_TYPES];
	struct list_head removed_maps[MAP__NR_TYPES];
	struct machine   *machine;
};

#endif /* __PERF_MAP_H */
