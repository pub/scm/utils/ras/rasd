#include <linux/perf_event.h>

#include "ras.h"

struct trace_event {
	struct pevent		*pevent;
	struct plugin_list	*plugin_list;
};

struct event_format *trace_event__tp_format(const char *sys, const char *name);

void event_format__print(struct event_format *event, int cpu, void *data,
			 int size);
