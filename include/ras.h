#ifndef __RAS_H__
#define __RAS_H__

/*
 * Put stuff from perf.h here so as not to copy *everything* over
 */

#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <inttypes.h>
#include <poll.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <linux/bitops.h>
#include <linux/hash.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/perf_event.h>

#include <asm/bug.h>
#include <asm/posix_types.h>

#include "lib.h"
#include "sane_ctype.h"

#define PERF_ALIGN(x, a)        __PERF_ALIGN_MASK(x, (typeof(x))(a)-1)
#define __PERF_ALIGN_MASK(x, mask)      (((x)+(mask))&~(mask))

#define BUILD_ID_SIZE		20
#define MAX_NR_CPUS		256

#if defined(__x86_64__)
#define mb()		asm volatile("mfence" ::: "memory")
#define wmb()		asm volatile("sfence" ::: "memory")
#define rmb()		asm volatile("lfence" ::: "memory")
#define cpu_relax()	asm volatile("rep; nop" ::: "memory");

#elif defined(__arm__)
/*
 * Use the __kuser_memory_barrier helper in the CPU helper page. See
 * arch/arm/kernel/entry-armv.S in the kernel source for details.
 */
#define mb()		((void(*)(void))0xffff0fa0)()
#define wmb()		((void(*)(void))0xffff0fa0)()
#define rmb()		((void(*)(void))0xffff0fa0)()
#define cpu_relax()	((void(*)(void))0xffff0fa0)()

#elif defined(__aarch64__)
#define mb()		asm volatile("dmb ish" ::: "memory")
#define wmb()		asm volatile("dmb ishst" ::: "memory")
#define rmb()		asm volatile("dmb ishld" ::: "memory")
#define cpu_relax()	asm volatile("yield" ::: "memory")
#endif

#define FD(e, x, y) (*(int *)xyarray__entry(e->fd, x, y))
#define SID(e, x, y) xyarray__entry(e->sample_id, x, y)

static inline int
sys_perf_event_open(struct perf_event_attr *attr,
		      pid_t pid, int cpu, int group_fd,
		      unsigned long flags)
{
	int fd;

	fd = syscall(__NR_perf_event_open, attr, pid, cpu,
		     group_fd, flags);

#if 0
	if (unlikely(test_attr__enabled))
		test_attr__open(attr, pid, cpu, fd, group_fd, flags);
#endif

	return fd;
}

static inline __attribute__((const))
bool is_power_of_2(unsigned long n)
{
	return (n != 0 && ((n & (n - 1)) == 0));
}

static inline void *zalloc(size_t size)
{
	return calloc(1, size);
}

#define zfree(ptr) ({ free(*ptr); *ptr = NULL; })

struct branch_flags {
	u64 mispred:1;
	u64 predicted:1;
	u64 in_tx:1;
	u64 abort:1;
	u64 reserved:60;
};

/*
 * rasd specific
 */
struct perf_rasd {
	char	*sys;
	char	*name;
};

/* evsel.c */
union u64_swap {
	u64 val64;
	u32 val32[2];
};

unsigned int page_size;

struct option {
};

/* util.c */
int filename__read_str(const char *filename, char **buf, size_t *sizep);
void event_attr_init(struct perf_event_attr *attr);
#endif /* __RAS_H__ */
