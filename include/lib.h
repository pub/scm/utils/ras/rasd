#ifndef __RASD_LIB_H__
#define __RASD_LIB_H__

#define err(fmt, args...)                                               \
({                                                                      \
        fprintf(stderr, "ERROR: " fmt "\n", ##args);                    \
        exit(-1);                                                       \
})

#endif /* __RASD_LIB_H__ */
