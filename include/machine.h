#ifndef __PERF_MACHINE_H
#define __PERF_MACHINE_H

#include "ras.h"

#include <linux/rbtree.h>

#include "map.h"

struct machine {
	struct rb_node    rb_node;
	pid_t             pid;
	u16               id_hdr_size;
	char              *root_dir;
	struct rb_root    threads;
	struct list_head  dead_threads;
	struct thread     *last_match;
	struct list_head  user_dsos;
	struct list_head  kernel_dsos;
	struct map_groups kmaps;
	struct map        *vmlinux_maps[MAP__NR_TYPES];
/* 	symbol_filter_t   symbol_filter; */
};

#endif /* __PERF_MACHINE_H */
